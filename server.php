<?php

use React\Socket\ConnectionInterface;
use React\Socket\SocketServer;
use App\ConnectionsPool;

require 'vendor/autoload.php';

$server = new SocketServer('127.0.0.1:8000');
$connectionPool = new ConnectionsPool();

$server->on('connection', function (ConnectionInterface $connection) use ($connectionPool){
    $connectionPool->add($connection);
});