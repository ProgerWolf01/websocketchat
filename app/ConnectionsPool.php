<?php

namespace App;

use React\Socket\ConnectionInterface;
use SplObjectStorage;

class ConnectionsPool
{
    private SplObjectStorage $connections;
    public function __construct() {
        $this->connections = new SplObjectStorage();
    }

    /**
     * @param ConnectionInterface $connection
     * @return void
     */
    public function add(ConnectionInterface $connection): void
    {
        $connection->write("Welcome here \n");
        $this->connections->attach($connection);
        $this->sendToAll("A new user joined chat \n", $connection);

        $connection->on('data', function ($data) use ($connection){
            $this->sendToAll($data, $connection);
        });

        $connection->on('close', function () use ($connection) {
            $this->sendToAll("A user leaves the chat \n", $connection);
            $this->connections->detach($connection);
        });
    }

    /**
     * @param string $massage
     * @param ConnectionInterface $expect
     * @return void
     */
    private function sendToAll(string $massage, ConnectionInterface $expect): void
    {
        /** @var ConnectionInterface $connection */
        foreach ($this->connections as $connection) {
            if ($connection != $expect) {
                $connection->write($massage);
            }
        }
    }
}